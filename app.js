const express = require('express')
const nunjucks = require('nunjucks')

var db= [
  {sms:"Lorem Ipsum is simply dummy text ofwewewew the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  date:"13/12/12",
  id: "0"
  },
  {sms:"MENSAJE DOS text of the printing and typweweweesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  date:"13/12/12",
  id: "1"
  },
];
var app = express()

nunjucks.configure('views', {
  autoescape: true,
  express: app
})

app.use(express.urlencoded({extended:false}));
app.use(express.json());
app.use("/hola",express.static('public'))


const PORT = '8000'


app.get('/', function(req, res) {
    res.render('index.html',{mensaje: db});
});
app.get('/mensaje/new', function(req, res) {
  res.render('form.html');
});
app.get('/mensaje/:id', function(req, res) {

  res.render('mensaje.html',{mensaje: db[req.params.id]});
});
app.post('/enviado', function(req, res) {
  let nuevomensaje={};
  nuevomensaje.sms=req.body.mensaje;
  nuevomensaje.date= (new Date()).toLocaleString('en-GB', { timeZone: 'UTC' });
  nuevomensaje.id= db.length;
  db.push(nuevomensaje);

  res.render('enviado.html');
});



app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}...`)
})

